import React, { FC } from "react";
import imgAdsDef from "@/images/banner.jpg";
import Image, { StaticImageData } from "next/image";

export interface SectionAdsProps {
  className?: string;
  imgAds?: string | StaticImageData;
}

const SectionAds: FC<SectionAdsProps> = ({
  className = "",
  imgAds = imgAdsDef,
}) => {
  return (
    <a
      href="/#"
      className={`nc-SectionAds block text-center mx-auto ${className}`}
    >
      <Image className="w-full h-56 mt-10 rounded-sm" src={imgAds} alt="ads" />
    </a>
  );
};

export default SectionAds;
