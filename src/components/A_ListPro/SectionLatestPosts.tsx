"use client";
import React, { FC, useState, useEffect } from "react";
import Card3 from "@/components/Card3/Card3";
import Heading from "@/components/Heading/HeadingList";
import Heading1 from "@/components/Heading/HeadingList1";
import WidgetTags from "@/components/WidgetTags/WidgetTags";
import { DEMO_POSTS } from "@/data/posts";
import { PostDataType } from "@/data/types";
import WidgetCategories from "@/components/WidgetCategories/WidgetCategories";
import WidgetAuthors from "@/components/WidgetAuthors/WidgetAuthors";
import WidgetPosts from "@/components/WidgetPosts/WidgetPosts";
import Pagination from "@/components/Pagination/Pagination";
import ButtonPrimary from "@/components/Button/ButtonPrimary";
import SectionAds from "@/components/Sections/SectionAds";
import Card4 from "@/components/Card4/Card4";
import Card7 from "@/components/Card7/Card7";
import Card9 from "@/components/Card9/Card9";
import Card10 from "@/components/Card10/Card10";
import Card11 from "@/components/Card11/Card11";
import Card14 from "@/components/Card14/Card14";
import Cart from "./Cart";

const postsDemo: PostDataType[] = DEMO_POSTS.filter((_, i) => i > 7 && i < 15);

interface SectionLatestPostsProps {
  posts?: PostDataType[];
  gridClassName?: string;
  className?: string;
  heading?: string;
  heading1?: string;
  postCardName?:
    | "card3"
    | "card4"
    | "card7"
    | "card9"
    | "card10"
    | "card11"
    | "card14";
}

const SectionLatestPosts: FC<SectionLatestPostsProps> = ({
  posts = postsDemo,
  postCardName = "card3",
  heading = "All Apps >",
  heading1 = "All Apps >",
  gridClassName = "gap-4 md:gap-4",
  className = "",
}) => {
  const renderCard = (post: PostDataType, index: number) => {
    // ... (your switch statement)
  };

  const [isWidgetVisible, setWidgetVisibility] = useState(false);

  const toggleVisibility = () => {
    console.log("Toggling visibility");
    setWidgetVisibility(!isWidgetVisible);
  };
  
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className={`nc-SectionLatestPosts relative ${className} `}>
      <div className="flex flex-col lg:flex-row">
      {!isMobile && (
          <div className="space-y-7 px-4 lg:px-10 xl:px-0 fixed left-16  w-1/6">
            <WidgetAuthors className="w-56"/>
          </div>
        )}

{isMobile && isWidgetVisible && (
  <div className="w-52 h-full border-solid border-1 border-black rounded-lg absolute top-14 left-0 z-50">
    <WidgetAuthors />
  </div>
)}



        <div
          className={`w-5/6 relative  ${
            isMobile ? "lg:w-full" : "lg:w-full"
          } xl:pe-14 xl:ml-44`}
        >
          <div className="w-screen h-14 -ml-4 ">


            {isMobile && isWidgetVisible && (
              <div className="fixed top-20 w-full h-14 border-y-2 border-solid bg-gray-50">
                <Heading1 onClick={toggleVisibility}>{heading}</Heading1>
              </div>
            )}


            <div className="fixed top-20 w-full h-14 border-y-2 border-solid bg-gray-50">
            <Heading1 onClick={toggleVisibility}>{heading}</Heading1>
            </div>

          </div>

          <SectionAds />

          <div
            className={`relative grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 mt-8 ml-2`}
          >
            {posts
              .filter((_, i) => i > 0)
              .map((p, index) => (
                <Cart key={index} post={p} />
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionLatestPosts;
