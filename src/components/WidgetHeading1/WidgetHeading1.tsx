"use client";
import { CustomLink } from "@/data/types";
import Link from "next/link";
import React, { FC, useState, useEffect } from "react";

export interface WidgetHeading1Props {
  className?: string;
  title: string;
}

const WidgetHeading1: FC<WidgetHeading1Props> = ({
  
  className = "",
  title,
}) => {

  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);


  return (
    <div
      className={`nc-WidgetHeading1 flex items-center justify-between p-4 xl:p-5 border-b border-neutral-200 dark:border-neutral-700 ${className}`}
    >
      {!isMobile && (
      <h2 className="text-xs text-neutral-900 dark:text-neutral-100 font-semibold flex-grow -ml-2">
        {title}
      </h2>
      )}

      {isMobile && (
      <h2 className="text-xs text-neutral-900 dark:text-neutral-100 font-semibold flex-grow -ml-5">
        {title}
      </h2>
      )}
    </div>
  );
};

export default WidgetHeading1;
