import React from "react";
import Describe from "@/components/A_detailsPro/Describe";
import Additional from "@/components/A_detailsPro/Additional";
import imgAdsDef from "@/images/banner.jpg";
import Image from "next/image";

// DEMO DATA
import { DEMO_POSTS } from "@/data/posts";

// DEMO POST FOR MAGAZINE SECTION
const POSTS = DEMO_POSTS;
const MAGAZINE1_POSTS = POSTS.filter((_, i) => i >= 0 && i < 8);
const MAGAZINE2_POSTS = DEMO_POSTS.filter((_, i) => i >= 0 && i < 7);

const PageHomeDemo2: React.FC = () => {
  return (
    <div className="nc-PageHomeDemo2 relative">
      <div className="relative">
        <a href="/#" className="nc-SectionAds block text-center mx-auto">
          <Image
            className="w-full h-96 rounded-lg"
            src={imgAdsDef}
            alt="ads"
            layout="responsive" // Thêm layout responsive cho hình ảnh
          />
        </a>
      </div>

      <div className="relative">
        <Describe />
      </div>

      <div className="relative">
        <Additional />
      </div>
    </div>
  );
};

export default PageHomeDemo2;