import React, { FC } from "react";
import Card2 from "@/components/A_ListBlog/Card2";
import { SectionMagazine1Props } from "./SectionMagazine1";
import Heading1 from "./Heading1";
import Card11 from "@/components/A_ListBlog/Card11";

export interface SectionMagazine2Props extends SectionMagazine1Props {}

export interface SectionMagazine2Props {
  className?: string;
  heading: string;
  subHeading: string;
}

const SectionMagazine2: FC<SectionMagazine2Props> = ({
  posts,
  heading = "Suggestions for discovery",
  subHeading = "Popular places to recommends for you",
  className = "",
}) => {
  return (
    <div className={`nc-SectionMagazine2 ${className}`}>

      <Heading1 desc={subHeading} isCenter>
        {heading}
      </Heading1>


      {!posts.length && <span>Nothing we found!</span>}
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6 mt-6">
        <div className="grid gap-6">
          {posts
            .filter((_, i) => i < 3 && i > 0)
            .map((item, index) => {
              return (
                <Card11 ratio="aspect-w-5 aspect-h-3" key={index} post={item} />
              );
            })}
        </div>
        <div className="lg:col-span-2">
          {posts[0] && <Card2 size="large" post={posts[0]} />}
        </div>
        <div className="grid grid-cols-1 gap-6 md:grid-cols-2 xl:grid-cols-1 md:col-span-3 xl:col-span-1">
          {posts
            .filter((_, i) => i < 4 && i >= 2)
            .map((item, index) => {
              return (
                <Card11 ratio="aspect-w-5 aspect-h-3" key={index} post={item} />
              );
            })}
        </div>
      </div>
</div>
  );
};

export default SectionMagazine2;
