import React from "react";
import { DEMO_POSTS } from "@/data/posts";
import SectionLatestPosts from "@/components/A_ListPro/SectionLatestPosts";

// DEMO DATA
const POSTS = DEMO_POSTS;

// DEMO POST FOR MAGAZINE SECTION
const MAGAZINE1_POSTS = POSTS.filter((_, i) => i >= 0 && i < 8);
const MAGAZINE2_POSTS = DEMO_POSTS.filter((_, i) => i >= 0 && i < 7);
//

const PageHomeDemo2: React.FC = () => {
  return (
    <div className="nc-PageHomeDemo2 relative">
      <div className="container relative">
        <SectionLatestPosts
          className="pb-16 lg:pb-28"
          postCardName="card14"
          posts={DEMO_POSTS.filter((_, i) => i >= 2 && i < 12)}
        />
      </div>
    </div>
  );
};

export default PageHomeDemo2;
