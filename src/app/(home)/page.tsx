import React from "react";
import Product from "@/components/A_Product-sl/product";
import Blog from "@/components/Blog/Blog";
import RightNav1 from "@/components/A_Product-sl/RightNav1";
import { DEMO_AUTHORS } from "@/data/authors";
import { DEMO_POSTS_NEWS } from "@/data/posts";

const MAGAZINE1_POSTS = DEMO_POSTS_NEWS.filter((_, i) => i >= 8 && i < 16);

const Homepage: React.FC = () => {
  return (
    <div className="container mx-auto">
      <div className="flex flex-col md:flex-row">
        <div className="w-full md:w-4/5">
          <div className="overflow-hidden">
            <div className="mx-4 md:mx-10 mb-6 md:mb-0">
              <Product
                heading="Suggested"
                subHeading=""
                authors={DEMO_AUTHORS.filter((_, i) => i < 10)}
              />
            </div>

            <div className="mx-4 md:mx-10 mb-6 md:mb-0">
              <Product
                heading="Text to image with Adobe Express"
                subHeading=""
                authors={DEMO_AUTHORS.filter((_, i) => i < 10)}
              />
            </div>

            <div className="mx-4 md:mx-10 mb-6 md:mb-0">
              <Product
                heading="Recent"
                subHeading=""
                authors={DEMO_AUTHORS.filter((_, i) => i < 10)}
              />
            </div>

            <div className="mx-4 md:mx-10 mt-10">
              <Blog posts={MAGAZINE1_POSTS} />
            </div>
          </div>
        </div>

        <div className=" md:ml-36 mt-3 md:mt-0">
          <div className="md:fixed">
            <RightNav1 />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Homepage;