import React, { ReactNode } from "react";
import SingleContent from "./SingleContent";
import SingleRelatedPosts from "./SingleRelatedPosts";

const layout = ({ children }: { children: ReactNode }) => {
  return (
    <div>
      {children}

      {/* SINGLE MAIN CONTENT */}
      <div className="container mt-10">
        <SingleContent />
      </div>

      {/* RELATED POSTS */}
      <div className="container"><SingleRelatedPosts /></div>
      
    </div>
  );
};

export default layout;
